 # Programación declarativa y funcional.
## Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)

```plantuml
@startmindmap
<style>
 node {
  
  BackgroundColor #FFFE9B
  LineColor #0F0F0F
 }
 arrow{
  LineColor black
 }
 </style>
*[#F9E3DD]  <b>Programación declarativa
 *_ Es un
  * Estilo de programación/paradigma. 
   *_ consiste en
    * Abandonar las secuencias de ordenes.
     *_ Que
      * Gestionan la memoria del ordenador. 
    *_ Surge como
     * Reacción de algunos problemas.
    *_ Lleva consigo
     * Programación clásica.
     * Programación imperativa.
     * El programador se ve obligado\n a dar excesos de detalles.
      *_ Sobre
       * Los cálculos que se requieran realizar. 
    *_ La idea es
     * Liberarse de tener que detallar\n específicamente el control de la gestión de\n memoria en el ordenador. 
      *_ Y utilizar
       * Otros recursos, a un nivel más alto
        *_ Y
         * Cercano a la forma de\n pensar del programador.
   *_ Ventajas
    * Programas más cortos
    * Faciles de depurar.
    * faciles de realizar.
    * Tiempo de modificación.
    * El tiempo de desarrollo de una\n aplicación es una milésima parte\n con un lenguaje convencional\n imperativo. 
   
 * Variantes principales
  *_ Segun el tipo de lenguaje o recursos\n que se utilicen.
   *[#DDF9F9] Programación Funcional
    * Aplicación de funciones a los datos.
     *_ se hace mediante
      * Reglas de reescritura y\n simplificación de expresiones. 
       *_ Asi se puede 
        * Alcanzar un nivel de descripción superior.
    *_ Recurre al lenguaje
     * Que utilizan los matemáticos
      *_ En particular
       * Lenguaje que describen funciones. 
    *_ Se utiliza 
     * Razonamiento ecuacional.
      * Son útiles para hacer programas.
       * Datos de entrada.
       * Datos de salida.
    * No se hace una gestión detallada de memoria.
    *_ Ventajas
     * Funciones de orden superior. 
      * Puedes definir funciones\n que actúan a su vez sobre funciones. 
    *_ Características fundamentales.
     * Existencia de funciones de orden superior.
      * Aplicar programas a otros programas.
       * Sucesión infinita. 
     * Evaluación perezosa.
      * Solo se evalúa aquello que es\n estrictamente necesario
       *_ Para
        * Hacer cálculos posteriores.
      * Las explicaciones no tienen\n por qué llevar una ejecución exhaustiva. 
       * Ejecuta solo aquello que necesita procesar\n para dar respuesta a una pregunta.



   *[#DDF9F9] Programación Lógica
    * Se acude a la lógica de predicados\n de primer orden.
     * Relaciones entre objetos que se definen. 
     * No establece un orden entre datos.
      *_ De
       * Entrada.
       * Salida.
      * Permite ser más declarativos. 
    *_ Utiliza
     * La demostración automática para programar.
     * Conjunto de hechos y reglas.
    
    * Se basa en el modelo de la demostración\n de la lógica.
     * Se utilizan como expresiones
      *_ De 
       * Los programas.
       * Axiomas.
       * Reglas de inferencia.
    
 *[#81FCBC] Programación orientada a inteligencia artificial. 
  *[#F2BFF6] Prolog
   * Lenguaje estándar.
   *_ Implementa concepto de
    * Programación lógica.
  *[#F2BFF6] Lisp
   *_ Lenguaje que tiene relación con 
    * La programación funcional.
   * Modelo de programación imperativa. 
   * Lenguaje hibrido. 

 * Haskell
  * Lenguaje estándar.
 
 * Materiales de lectura
  * Programación funcional. 
   *_ Autor
    * Jeroen Fokker. 
  * Lenguajes de programación: conceptos\n y constructores
   * Capitulo 8
    *_ Autor
     * Ravi Sethi	



@endmindmap
```

## Lenguaje de programación funcional.
 
```plantuml
@startmindmap
<style>
 node {
  
  BackgroundColor #FFFE9B
  LineColor #0F0F0F
 }
 arrow{
  LineColor black
 }
 </style>
*[#6FFA89] <b>Lenguaje de programación funcional.
 *[#81FCBC] Paradigmas de programación.
  * Modelo de computación en que los diferentes\n lenguajes dotan de semántica a los diferentes programas. 
   *_ Basados en
    * El modelo de Von Neumann
     *_ Los programas debían almacenarse en
      * La misma maquina antes de ser ejecutados. 
       * Ejecutados secuencialmente.
       * Se podría modificar según el estado del cómputo.
     * Se podrían modificar mediante\n una instrucción de asignación.

   *[#DDF9F9] Programación orientado a objetos. 
    *_ Consiste en 
     * Pequeños trozos de códigos.
      *_ Se componen de 
       * Instrucciones que se ejecutan\n secuencialmente. 

   *[#DDF9F9] Paradigma de programación lógico.
    *_ Se basa en
     * La lógica simbólica. 
    *_ Se forma mediante
     * conjunto de sentencias. 
      * Define lo que es verdad.
       * Lo que es conocido para un\n programa con respecto a un problema. 
    * Se puede resolver el problema\n sin decir la secuencia de pasos a realizar. 

  
  
   *[#DDF9F9] Programación funcional
    * Todo gira alrrededor de las funciones.
    *[#F4DDF9] Lambda calculo.
     *_ Se desarrollo en 
      * 1930 
       *_ Por 
        * Church Y Kleene.
     *_ Inicialmente se pensó como
      * Un sistema para estudiar conceptos.
       *_ como son
        * Función.
        * Recursividad.
        * Aplicación de funciones. 

     * Se obtuvo un sistema computacional potente.
      * Equivalente al de Von Neumman. 
     * Lógica combinatoria. 
      * Variante del lambda calculo. 
       *_ Desarrollada por 
        * Haskell Brooks Curry
      * Fueron los cimientos sobre los que\n permitió a Peter Landín modelar un lenguaje de programación. 
       * sentó las bases para la\n programación funcional.

      *_ Característica
       * Una función con n parámetros y que\n devuelve un parámetro de salida.
        *_ puede ser vista como
         * Una función que tiene un único parámetro\n de entrada
          *_ y que 
           * Devuelve una función con\n n-1 parámetros de entrada.
    *_ Concepción
     * Puramente matemática.
    *_ Consecuencias
     * Concepto de asignación
     * Bucles.
      * son resueltas de otra forma.
       * Mediante la recursión. 

     * Concepto de variable.
      * solo sirven para referirse a los parámetros\n de entradas de las funciones. 

     * Concepto de constante.
      * Sigue existiendo.
       *_ pero
        * Equiparan a funciones. 

      * Función constante
       * Si una función devuelve siempre\n el mismo resultado.
        * Si devuelve el mismo resultado tiene que ser\n independientemente de los parámetros que reciba.

     * Currifizacion.
      *_ en honor a 
       * Haskell B. Curry.
        *_ es una función que
         * Tiene varios parametros de entrada.
          * No solo devuelve una salida.
           *_ sino que 
            * Devuelve varias salidas, diferentes funciones.
     * Aplicación parcial
    * Ventajas
     * Sin estado de cómputo.
      * Los programas solo dependen\n de los parámetros\n de entrada. 
       *_ se conoce como
        * Transparencia referencial.
         * Los resultados de las funciones son\n independientes del orden en el que se\n realicen los cálculos.
          * El orden no es relevante. 
     * Función de orden superior
      * Todo gira alrededor de las\n funciones.
       * Ciudadanos de primera clase. 
        * Las funciones pueden ser parámetros\n de otras funciones. 
    *_ ¿Como se evalúan las funciones?
     *[#F6D4A9] Evaluación impaciente.
      *_ consiste en
       * Evaluar las expresiones desde\n dentro hacia afuera.

     *[#F6D4A9] Evaluación no estricta
      * evalúa desde fuera hacia dentro.
       *_ es
        * Mas complicada de implementar. 
      * Una función que no necesita conocer\n el valor uno de sus parámetros para\n devolver un resultado.
      *_ Se implementa en algunos\n lenguajes imperativos
       * Evaluación en corto circuito

      *[#D1F6A9] Evaluación perezosa.
       * Nunca trabaja de más,\n pero tiene buena memoria.
        *[#B0FC81] Memoización
         * 1968 
          *_ por
           * Donald Michie 
         * Almacenar el valor de una\n expresión cuya evaluación ya a sido realizada.
          * Si se necesita evaluar algo, se evalúa,\n pero cuando ya se ha evaluado ya no\n se tiene porque volver a evaluar.
           * La evaluación solo se realiza una única vez.
            *_ Se conoce como
             * Paso por necesidad.

    *_ Utilidades
     * Realizar prototipos rápidamente.
     * Eficiencia con respecto a la programación.

    *_ Desventajas
     * Costo computacional mayor
    *_ Campos aplicables
     * Cualquier campo
      *_ Ejemplo
       * Juegos
@endmindmap
```

